#!/bin/bash

echo "Upload files to all servers.....\n"

./UploadFiles.sh ec2-35-182-177-159.ca-central-1.compute.amazonaws.com 1
./UploadFiles.sh ec2-35-183-30-64.ca-central-1.compute.amazonaws.com 2
./UploadFiles.sh ec2-35-183-51-51.ca-central-1.compute.amazonaws.com 3
./UploadFiles.sh ec2-35-182-48-152.ca-central-1.compute.amazonaws.com 4
./UploadFiles.sh ec2-35-182-242-213.ca-central-1.compute.amazonaws.com 5
./UploadFiles.sh ec2-35-182-187-139.ca-central-1.compute.amazonaws.com 6
./UploadFiles.sh ec2-35-182-237-33.ca-central-1.compute.amazonaws.com 7
./UploadFiles.sh ec2-35-183-23-132.ca-central-1.compute.amazonaws.com 8
./UploadFiles.sh ec2-35-182-231-113.ca-central-1.compute.amazonaws.com 9
./UploadFiles.sh ec2-35-182-228-6.ca-central-1.compute.amazonaws.com 10
./UploadFiles.sh ec2-35-182-77-110.ca-central-1.compute.amazonaws.com 11
./UploadFiles.sh ec2-35-182-53-73.ca-central-1.compute.amazonaws.com 12
./UploadFiles.sh ec2-35-182-193-115.ca-central-1.compute.amazonaws.com 13
./UploadFiles.sh ec2-35-183-39-3.ca-central-1.compute.amazonaws.com 14
./UploadFiles.sh ec2-35-182-254-98.ca-central-1.compute.amazonaws.com 15
./UploadFiles.sh ec2-35-183-39-50.ca-central-1.compute.amazonaws.com 16
./UploadFiles.sh ec2-35-182-248-177.ca-central-1.compute.amazonaws.com 17
./UploadFiles.sh ec2-35-182-142-168.ca-central-1.compute.amazonaws.com 18
