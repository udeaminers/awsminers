#!/bin/bash

urldir=$1
#configFile=$1
id=$2

# Upload files to machine in urldir
echo "================================== Configuring machine $id ====================================="
# scp -o "StrictHostKeyChecking no" -i 'prueba_miner.pem' InstallXMRig.sh ubuntu@$urldir":~"
# ssh -i 'prueba_miner.pem' ubuntu@$urldir "chmod +x InstallXMRig.sh"
# ssh -i 'prueba_miner.pem' ubuntu@$urldir "./InstallXMRig.sh"
# scp -o "StrictHostKeyChecking no" -i 'prueba_miner.pem' mining.sh ubuntu@$urldir":~"
#scp -o "StrictHostKeyChecking no" -i 'prueba_california.pem' mining.sh ubuntu@$urldir":~"

#configure config.json to p3.2xlarge instances with AMI
# sed "s/\_[0-9][0-9]*\//\_$id\//g" config2.json -i
sed "s/\_[0-9][0-9]*\//\_$id\//g" configCan.json -i
# scp -i 'prueba_miner.pem' config2.json ubuntu@$urldir":~/config.json"
#scp -i 'prueba_california.pem' config2.json ubuntu@$urldir":~/config.json"
#scp -o "StrictHostKeyChecking no" -i 'prueba_california.pem' config2.json ubuntu@$urldir":~/xmrig/build/config.json"
scp -o "StrictHostKeyChecking no" -i 'prueba_miner.pem' configCan.json ubuntu@$urldir":~/xmrig/build/config.json"
#scp -i 'prueba_miner.pem' config2.json ubuntu@$urldir":~/xmrig/build/config.json"

echo "******************************"
echo "RUN $id ..... DONE"
echo "******************************"
