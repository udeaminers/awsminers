#!/bin/bash

# Kill process
pgrep "xmrig" | xargs kill -9

# update config.json
#sed 's/"safe": true/"safe": false/' ~/xmrig/build/config.json -i

# top
#top -n 1 -b > top_out.txt
#head top_out.txt
