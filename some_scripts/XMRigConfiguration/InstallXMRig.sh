#!/bin/bash

echo "Updating..."
sudo apt-get -y update


echo "Installing XMRIG"

# download packages
sudo apt-get -y install git build-essential cmake libuv1-dev libmicrohttpd-dev

# download util packages
sudo apt install -y htop atop

# clone repository
git clone https://github.com/xmrig/xmrig.git

# Doing the installation
cd xmrig
mkdir build
cd build
cmake ..
make
