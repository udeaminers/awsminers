#!/bin/bash

dns=$1
id=$2

shEthermine="mining_ethminer_ethermine.sh"
configfile="configure_ethminer_server.sh"
echo "================================== Configuring machine $id ====================================="
#add worker id to miner
sed "s/\_[0-9][0-9]*/\_$id/g" $shEthermine -i
#upload file
scp -o "StrictHostKeyChecking no" -i '../prueba_miner.pem' $shEthermine $configfile ubuntu@$dns":~"
# ---- WITH AMI -------
#scp -o "StrictHostKeyChecking no" -i '../prueba_miner.pem' $shEthermine ubuntu@$dns":~/ethminer/"
#run configuration
#ssh -i 'prueba_miner.pem' ubuntu@$dns "./configure_ethminer_server.sh"
# running miner
#ssh -i 'prueba_miner.pem' ubuntu@$dns "./mining_ethminer_ethermine.sh"
#ssh -i 'prueba_miner.pem' ubuntu@$dns "mv $shEthermine ethminer/"
