#!/bin/bash

# updating system
sudo apt update
sudo apt -y install htop

# download miner
wget https://github.com/ethereum-mining/ethminer/releases/download/v0.14.0rc2/ethminer-0.14.0rc2-Linux.tar.gz

# unpack
tar -xzvf ethminer-0.14.0rc2-Linux.tar.gz
mv bin/ ethminer/
mv mining*.sh ethminer/
