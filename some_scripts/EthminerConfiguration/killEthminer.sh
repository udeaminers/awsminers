#!/bin/bash

urldir=$1
id=$2
echo "================================== Kill process in machine $id ====================================="
ssh -i 'prueba_miner.pem' ubuntu@$urldir " pgrep ethminer | xargs kill -9"
