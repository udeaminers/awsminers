#!/bin/bash

# >>>>>>>>>>>>>>>>>>>>>
# HOW TO USE
# >>>>>>>>>>>>>>>>>>>>>
# One file: $ ./GetHashSecByFile.sh ONE edison|cesar <namefile>.txt
# All files: $ ./GetHashSecByFile.sh ALL edison (only for edison's files)



# Get initial information to work with
# specific file type
#----------------------------------------
mode=$1 #(ONE or ALL)
worker=$2
ifilename=${3:-none} # taken from https://www.gnu.org/software/bash/manual/bashref.html#Shell-Parameter-Expansion
accepted=${4:-none}

#ofilename=$3
#toDo=$4

if [[ "$worker" = "cesar" ]]; then
  #  ====== CCMINER ======
  # Filter H/s from file connected to pool
  #grep -E "yes!|yay" $ifilename | awk -F "," '{print $2}' | awk '{print $1}' > ${ifilename%.txt}.dat

  #  ====== XMRIG ======
  if [[ "$mode" != "ALL" ]]; then
    grep "H/s" $ifilename | awk -F "/15m" '{print $2}' | awk '{print $1}' > ${ifilename%.txt}.dat
  else
    for file in $(ls *XMRig*.txt)
    do
      grep "H/s" $file | awk -F "/15m" '{print $2}' | awk '{print $1}' > ${file%.txt}.dat
      echo "file ${file%.txt}.dat created...."
    done
  fi

elif [[ "$worker" = "edison" ]]; then
  # Filter H/s from file
  # based in: http://tldp.org/LDP/abs/html/string-manipulation.html
  if [[ "$mode" != "ALL" ]]; then
    grep "XMR hashrate" $ifilename | awk -F "hashrate: " '{print $2}'| cut -d" " -f1 > ${ifilename%.txt}.dat
  else
    for file in $(ls *minergate-cli*)
    do
      grep "XMR hashrate" $file | awk -F "hashrate: " '{print $2}'| cut -d" " -f1 > ${file%.txt}.dat
      echo "file ${file%.txt}.dat created...."
    done
  fi
else
  echo "Usuarios incorrectos"
fi
