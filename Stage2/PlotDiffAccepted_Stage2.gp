#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================

#>>>>>>>>>>>>>>>>>>>>>>>
# HOw TO USE
#>>>>>>>>>>>>>>>>>>>>>>>
# For one file :  $ gnuplot -c plotMinerHashRate.gp 1 <namefile>.dat
# For two files : $ gnuplot -c plotMinerHashRate.gp 2 <namefile1>.dat <namefile2>.dat (only for ccminers)

# COLOR DEFINITIONS
#--------------------------------------------------------------------------------------
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 3 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 3 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 3 # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 3 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 3 # medium purple

# FUNCTIONS
#--------------------------------------------------------------------------------------
getField(name,loc) = sprintf('echo "%s" | cut -d"_" -f%d',name,loc)

# ARGUMENTS
#--------------------------------------------------------------------------------------
#nfiles = ARG1

ifile1 = ARG1
miner1 = system(getField(ifile1,3))
machine = system(getField(ifile1,7)." | awk -F'.' '{print $1}'")
ngpu = system(getField(ifile1,5))
ncpu = system(getField(ifile1,4))
pool = system(getField(ifile1,6))


# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set terminal pdf size 8,6 font 'Verdana,22' enhanced color lw 2 rounded
set output 'timediffAccepted_'.ncpu.'_'.ngpu.'_'.miner1.'_'.machine.'_'.pool.'.pdf'



# SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
set style line 11 lc rgb '#808080' lt 1
set border lw 1
#set border 3 back ls 11
#set tics nomirror out scale 0.75

#  *** Grid ***
set style line 12 lc rgb'#808080' lt 0 lw 1
set grid back ls 12


#  *** Axes Label ***
set xlabel "Number of accepted jobs" font ',26'
set ylabel "T_{i+1} - T_{i}  [min:sec]" font ',26'


#  **** Margins ***
#set rmargin 4
#set bmargin 4

#  *** Axes format ***
#set format x '%g'
nl = system("wc -l ".ifile1)
set xrange [0:nl]
set ydata time
set timefmt "%M:%S"
set format y "%M:%S"
set xtics font ",18"
set ytics font ",18"

#  *** KeyBox ***
#set key at 39,0.64 box font ',28' spacing 5 width 3 opaque
set key top right box font ',21' spacing 3 width 1 opaque
#set key horiz
#set key left top

# for stat time look this link
# https://stackoverflow.com/questions/35101917/gnuplot-statistics-with-time-data-converted-from-string-as-float-fails-as-sing

# PLOT EVERY FILE IN SEPARATED FOLDERS
#--------------------------------------------------------------------------------------

# TITLE
set title 'With '.miner1.' in '.machine.' ('.ncpu.','.ngpu.')'
plot ifile1 u 3 t 'difftime' w l ls 2 lw 1
  #pause -1
