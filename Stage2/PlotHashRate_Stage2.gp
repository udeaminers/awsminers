#==================================================================================================================
# SCRIPT GNUPLOT
#==================================================================================================================

#>>>>>>>>>>>>>>>>>>>>>>>
# HOw TO USE
#>>>>>>>>>>>>>>>>>>>>>>>
# For one file :  $ gnuplot -c plotMinerHashRate.gp 1 <namefile>.dat
# For two files : $ gnuplot -c plotMinerHashRate.gp 2 <namefile1>.dat <namefile2>.dat (only for ccminers)

# COLOR DEFINITIONS
#--------------------------------------------------------------------------------------
set border linewidth 1.5
set style line 1 lt 1 lc rgb '#D73027' lw 3 # red
set style line 2 lt 1 lc rgb '#1B7837' lw 3 # dark green
set style line 3 lt 1 lc rgb '#FDAE61' lw 3 # 'orange'
set style line 4 lt 1 lc rgb '#74ADD1' lw 3 # medium blue
set style line 5 lt 1 lc rgb '#9970AB' lw 3 # medium purple

# FUNCTIONS
#--------------------------------------------------------------------------------------
getField(name,loc) = sprintf('echo "%s" | cut -d"_" -f%d',name,loc)

# ARGUMENTS
#--------------------------------------------------------------------------------------
nfiles = ARG1

if(nfiles>1){
  ifile1 = ARG2
  ifile2 = ARG3
  miner1 = system(getField(ifile1,5))
  miner2 = system(getField(ifile2,5))
  machine = system(getField(ifile1,2))#." | awk -F'.' '{print $1}'")
  #print machine
} else {
  ifile1 = ARG2
  miner1 = system(getField(ifile1,5))
  machine = system(getField(ifile1,2))
}

# SET TERMINAL CONFIGURATION
#--------------------------------------------------------------------------------------
set terminal pdf size 8,6 font 'Verdana,22' enhanced color lw 2 rounded
if(nfiles>1){
  set output 'minerRate_'.nfiles.'miner_'.substr(miner1,0,strlen(miner1)-3).'_'.machine.'.pdf'
}else{
  ngpu = system(getField(ifile1,4))
  ncpu = system(getField(ifile1,3))
  pool = system(getField(ifile1,6))
  set output 'minerRate_'.ncpu.'_'.ngpu.'_'.miner1.'_'.machine.'_'.pool.'.pdf'
}

# DETERMINING STATISTIC OF FILES
#--------------------------------------------------------------------------------------
if(nfiles>1){
  # >>> File 1
  set print 'stat_'.miner1.'_'.machine.'.txt'
  stat ifile1 prefix 'St1'
  # >>> File 2
  set print 'stat_'.miner2.'_'.machine.'.txt'
  stat ifile2 prefix 'St2'
  # set print to default
  set print '-'
}else{
  # >>> File 1
  set print 'stat_'.miner1.'_'.ncpu.'_'.ngpu.'_'.machine.'.txt'
  stat ifile1 prefix 'St1'
  # set print to default
  set print '-'
}

# SET GENERAL (global) PARAMETERS OF THE PLOTS
#--------------------------------------------------------------------------------------

#  *** Axes ***
set style line 11 lc rgb '#808080' lt 1
set border lw 1
#set border 3 back ls 11
#set tics nomirror out scale 0.75

#  *** Grid ***
set style line 12 lc rgb'#808080' lt 0 lw 1
set grid back ls 12


#  *** Axes Label ***
set xlabel "Iteration every 2.5s" font ',28'
set ylabel "Hash per second" font ',28'


#  **** Margins ***
#set rmargin 4
#set bmargin 4

#  *** Axes format ***
#set format x '%g'
set xtics font ",20"
set ytics font ",20"
miny = floor(St1_min)-3
maxy = ceil(St1_max)
set yrange [ miny : maxy ]

#  *** KeyBox ***
#set key at 39,0.64 box font ',28' spacing 5 width 3 opaque
set key center right box font ',21' spacing 3 width 1 opaque
#set key horiz
#set key left top



# PLOT EVERY FILE IN SEPARATED FOLDERS
#--------------------------------------------------------------------------------------

if(nfiles>1){
  # Label with mean value
  #-------------------------------
  # >>> file1
  LABEL = sprintf("ccminer old Mean: %.3f H/s",St1_mean)
  set obj 1 rect at 0.5*St2_records,St1_mean size char strlen(LABEL)-5,char 1
  set obj 1 rect fc rgb "white" fillstyle solid 0.0 front
  set label 1  LABEL at 0.5*St2_records,St1_mean font ',18' front center
  # >>> file1
  LABEL = sprintf("ccminer new Mean: %.3f H/s",St2_mean)
  set obj 2 rect at 0.5*St2_records,St2_mean*1.05 size char strlen(LABEL)-5,char 1
  set obj 2 rect fc rgb "white" fillstyle solid 0.0 front
  set label 2  LABEL at 0.5*St2_records,St2_mean*1.05 font ',18' front center

  # TITLE OF PLOT
  set title 'Machine '.machine
  #set xrange [0:1000]
  plot "< tail -n +10 ".ifile1 u 1 t miner1 w l ls 3 lw 1,\
  "< tail -n +10 ".ifile2 u 1 t miner2 w l ls 2 lw 2

}else{
  # Label with mean value
  #-------------------------------
  # >>> file1
  LABEL = sprintf("Mean: %.3f H/s",St1_mean)
  set obj 1 rect at 0.5*St1_records,St1_mean size char strlen(LABEL)-3,char 1
  set obj 1 rect fc rgb "white" fillstyle solid 0.0 front
  set label 1  LABEL at 0.5*St1_records,St1_mean font ',18' front center

  # TITLE
  set title 'With '.miner1.' in '.machine.' ('.ncpu.','.ngpu.')'
  plot ifile1 u 1 t miner1 w l ls 5 lw 1
  #pause -1
}
