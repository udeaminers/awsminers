#!/bin/bash

# >>>>>>>>>>>>>>>>>>>>>
# HOW TO USE
# >>>>>>>>>>>>>>>>>>>>>
# One file: $ ./GetHashSecByFile.sh ONE edison|cesar <namefile>.txt
# All files: $ ./GetHashSecByFile.sh ALL edison (only for edison's files)



# Get initial information to work with
# specific file type
#----------------------------------------
filename=$1
#accepted=${4:-none} # taken from https://www.gnu.org/software/bash/manual/bashref.html#Shell-Parameter-Expansion

# references
#-------------------------------------------
# 1. https://unix.stackexchange.com/questions/178862/awk-output-the-second-line-of-a-number-of-dat-files-to-one-file
# 2. https://unix.stackexchange.com/questions/318008/how-to-subtract-dates-from-colums-using-awk/318015
# 3. https://www.unix.com/shell-programming-and-scripting/130727-how-subtract-adjacent-lines-single-column.html
# 4. https://www.linuxquestions.org/questions/programming-9/convert-total-no-of-seconds-in-the-format-hour-minutes-and-seconds-276427/
# 5. https://stackoverflow.com/questions/8019617/how-to-write-finding-output-to-same-file-using-awk-command

# Get info from fields of a filename
miner=$(echo $filename | cut -d"_" -f5 )
ngpu=$(echo $filename | cut -d"_" -f4 )
ncpu=$(echo $filename | cut -d"_" -f3 )
pool=$(echo $filename | cut -d"_" -f6 )
machine=$(echo $filename | cut -d"_" -f2 )

# output files
ofile1="time_accepted_"$miner"_"$ncpu"_"$ngpu".dat"
ofile2="difftime_accepted_"$miner"_"$ncpu"_"$ngpu"_"$pool"_"$machine".dat"

# Get time where job was accepted by the pool
grep "accepted" $filename | awk '{ print substr($2,0,length($2)-1) }' > $ofile1

# Get difference between time of accepted work
awk 'BEGIN{one=0;}{"date -d "$1" +%s"|getline time; diff=time-one;} \
     FNR>1 {printf "%-12s %-6s %-12s\n",$1,diff,strftime("%M:%S",diff)}{one=time}' $ofile1 > $ofile2
rm $ofile1
