records	377
invalid	0
blanks	0
blocks	1
outofrange	0
mean_y	42.580902
stddev_y	4450.139037
ssd_y	4456.052846
skewness_y	-19.240289
kurtosis_y	372.474643
adev_y	462.087343
sum_y	16053.000000
sum_sq_y	7466692571.000000
mean_err_y	229.193821
stddev_err_y	162.064505
skewness_err_y	0.126155
kurtosis_err_y	0.252310
min_y	-86103.000000
lo_quartile_y	90.000000
median_y	202.000000
up_quartile_y	377.000000
max_y	2297.000000
min_index_y	301
max_index_y	362
